<?php
    require_once ('./animal.php');
    class Frog extends Animal{
        public $legs=4;
        public function jump(){
         echo "hop hop <br>";
        }
    }
    
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>